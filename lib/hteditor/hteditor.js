// HTE - Human Text Editor
// Created by Joao Makray - http://joaomak.net/
// See hte.css for customization and usage instructions
//
// Config:

hte_folder="hteditor";

buttons=new Array(
'bold',
'italic',
'link',
'hr',
'image',
'quote',
'ul',
'ol',
'h2',
//'br',
//'help',
'Preview'
);

buttons_icons=new Array(
'bold',
'italic',
'link',
'minus',
'picture',
'quote-left',
'list-ul',
'list-ol',
'h-sign',
//'br',
//'question',
'eye-open'
);


// English
buttons_title=new Array(
'bold',
'italic',
'link',
'line',
'image',
'quote',
'list',
'numbers',
'title',
//'line break',
'help',
'preview',
'Pick',
'Select something first.',
'File description (optional)'
);


// Portugues

buttons_title_pt=new Array(
'negrito',
'itálico',
'link',
'linha',
'imagem',
'citação',
'lista',
'numerada',
'título',
//'quebra de linha',
'ajuda',
'ver',
'Escolha',
'Selecione algo primeiro.',
'Descrição do arquivo (opcional)'
);
if(_lang && _lang=='pt_BR') buttons_title=buttons_title_pt;

editorClass='markdown';

HTE_init();

/******************************************************************************/

//includeExtras(); // include extra js file and css file
//HTE_addEvent(window,"load",HTE_init);


function HTE_init(){
	converter = new Showdown.converter();
	var txta;
	var txtareas=document.getElementsByTagName('textarea');
	for(var i=0;i<txtareas.length;i++){
		txta=txtareas[i];
		var tid=txta.getAttribute('id');
		//alert(tid);
		if(tid==null || tid==''){
			var newid='HTA'+i;
			txta.setAttribute('id',newid);
		}else newid=tid;

		var txtclass=txta.className;
		if(txtclass.substring(0,editorClass.length)==editorClass ||
	 	   txtclass.substring(txtclass.length -editorClass.length)==editorClass)
				initEditor(newid, 'markdown');
	}
}
/* ie
function includeExtras(){
	var head = document.getElementsByTagName('head')[0];
	// javascript
	var jsfile = document.createElement("script");
	jsfile.setAttribute('src',hte_folder+"/showdown.js");
	jsfile.setAttribute('type',"text/javascript");
	head.appendChild(jsfile);
	// include CSS
	var cssfile = document.createElement("link");
	cssfile.setAttribute('rel',"stylesheet");
	cssfile.setAttribute('href',hte_folder+"/hte.css");
	cssfile.setAttribute('type',"text/css");
	head.appendChild(cssfile);
}*/
function getLink(inputId, syntax, callback){
    if(document.getElementById(inputId+'_link')){
		toggleLayer(inputId+'_link');
    }else{
	var input = document.getElementById(inputId+'_tools');
	var prompt = document.createElement("div");
	prompt.setAttribute('id', inputId+'_link');
	prompt.className= 'HTE_linkBox bordercolor';

	var label1=document.createElement("label");
	label1.appendChild(document.createTextNode('URL: '));
	prompt.appendChild(label1);
	// input
	var i = document.createElement("input");
	i.setAttribute('type', 'text');
	i.setAttribute('class', 'input-url form-control');
	i.setAttribute('size', '20');
	i.setAttribute('value', 'http://');
	i.setAttribute('name', inputId+'_linkText');
	i.setAttribute('id', inputId+'_linkText');
	prompt.appendChild(i);
	var ws=document.createTextNode(" ");

	prompt.appendChild(ws);

	//prompt.appendChild(document.createElement("hr"));
	//fileManager
	var a = document.createElement("button");
	a.className='files btn btn-default';
	a.onclick= function(){
		window.returnField = inputId+'_linkText';
		fileManager();
		return false;
	}
	var fm = document.createTextNode(buttons_title[11]);
	var span = document.createElement("span");
	span.appendChild(fm);
	a.appendChild(span);
	prompt.appendChild(a);

	// alt
	var altlabel = document.createTextNode(buttons_title[13]+":");
	var label2=document.createElement("label");
	label2.appendChild(altlabel);
	prompt.appendChild(label2);
	var a = document.createElement("input");
	a.setAttribute('type', 'text');
	a.setAttribute('class', 'input-descr form-control');
	a.setAttribute('size', '34');
	a.setAttribute('value', '');
	a.setAttribute('name', inputId+'_linkAlt');
	a.setAttribute('id', inputId+'_linkTAlt');
	prompt.appendChild(a);
	prompt.appendChild(document.createElement("br"));
	// ok
	var b = document.createElement("button");
	b.className='ok btn btn-default';
	//b.setAttribute('href','javascript://');
	b.onclick=function(){
		eval(syntax+"('"+callback+"','"+inputId+"');");
		this.parentNode.parentNode.removeChild(this.parentNode);
	};
	var txt = document.createTextNode('OK');
	//b.appendChild(txt);
	var span = document.createElement("span");
	span.appendChild(txt);
	b.appendChild(span);

	prompt.appendChild(b);
	// cancel
	c=document.createElement('button');
	c.className='cancel btn btn-default';
	//c.setAttribute('href','javascript://');
	c.onclick=function(){this.parentNode.parentNode.removeChild(this.parentNode);};
	var ctxt = document.createTextNode('Cancel');
	//c.appendChild(ctxt);
	var span = document.createElement("span");
	span.appendChild(ctxt);
	c.appendChild(span);
	prompt.appendChild(c);
	// do
	input.parentNode.insertBefore(prompt, input);
	i.focus();
    }
}
function initEditor(inputId, syntax){
	//alert(inputId);
	var input = document.getElementById(inputId);
	if($(input).hasClass('hte'))return false;
	else $(input).addClass('hte');
	input.onkeyup=function(){reloadPreview(inputId)};
	//input.style.display='none';

	// tools
	var tools = document.createElement("div");
 	tools.className = 'HTE_tools';
 	tools.setAttribute('id', inputId+'_tools');
 	//preview.onmousedown=function(){switchView(inputId)};
 	//input.style.display='block';
 	//tools.style.width=input.style.width;
	for(var i=0;i<buttons.length-1 ;i++){
	 	var a = document.createElement("a");
	 	a.setAttribute('href', "javascript:"+syntax+"('"+buttons[i]+"', '"+inputId+"')");
	 	a.setAttribute('title', buttons_title[i]);
	 	a.className = 'btn btn-default btn-sm HTE_'+buttons[i];
	 	//var txt = document.createTextNode(buttons_title[i]);
	 	//a.appendChild(txt);
	 	$(a).html('<i class="icon icon-'+buttons_icons[i]+'"></i> '+buttons_title[i]);
	 	//a.innerText=':'+buttons[i];
	 	tools.appendChild(a);
	}

	// switch view
	var toggle = document.createElement("a");
	toggle.setAttribute('href', "javascript:switchView('"+inputId+"')");
	toggle.setAttribute('title', buttons_title[i+1]);
	toggle.className='btn btn-default btn-sm HTE_toggle';
	//var prevLink = document.createTextNode(buttons[i]);
	//toggle.appendChild(prevLink);
	$(toggle).html('<i class="icon icon-eye-open"></i> '+buttons_title[i+1]);
	//input.parentNode.insertBefore(toggle, input);
	tools.appendChild(toggle);

	// preview
	var preview = document.createElement("div");
 	preview.className = 'HTE_preview';
 	preview.setAttribute('id', inputId+'_preview');
 	//preview.setAttribute('onmousedown', 'dragStart(event)');//onmousedown="dragStart(event)"
	input.parentNode.insertBefore(preview, input);

 	$(tools).append('<a href="#" onclick="return help()" class="btn btn-sm icon icon-question-sign help-link" title="'+buttons_title[9]+'"></a>');

	input.parentNode.insertBefore(tools, preview);
	reloadPreview(inputId);
}
ol=0;
function markdown(tipo,inputId) {
	var str = getSel(inputId);
	ol++;
	switch (tipo){
	case 'bold':
		txt = '**' + str + '**';
	break;
	case 'italic':
		txt = '_' + str + '_';
	break;
	case 'hr':
		txt = '\n\n--------\n';
	break;
	case 'quote':
		txt = '\n> ' + str ;
	break;
	case 'link':
		if(!str){
			alert(buttons_title[12]);
			return;
		}
		//var my_link = prompt("Enter URL:","http://");
		getLink(inputId, 'markdown', 'setLink');
		return;
	break;
	case 'setLink':
		var linkInput = document.getElementById(inputId+'_link');
		var link = linkInput.getElementsByTagName('input')[0].value;
		var title = linkInput.getElementsByTagName('input')[1].value;
  		if (link != null) {
			txt = '['+str+'](' + link;
			if(title)txt += ' "'+title+'"';
			 txt += ')';
  		}else return;
		toggleLayer(inputId+'_link');
	break;
	case 'image':
		getLink(inputId, 'markdown', 'setImage');
		return;
	case 'setImage':
		//var alt = prompt(buttons_title[12]+":","");
		var linkInput = document.getElementById(inputId+'_link');
		var link = linkInput.getElementsByTagName('input')[0].value;
		var alt = linkInput.getElementsByTagName('input')[1].value;
  		if (alt != null) {
			txt = '!['+alt+']('+link+')';
  		}else return;
		toggleLayer(inputId+'_link');
	break;
	case 'ul':
		txt = '\n* ' + str ;
	break;
	case 'ol':
		txt = '\n'+ol+'. ' + str ;
	break;
	case 'h2':
		var sz=str.length;
		txt = str + '\n';
		for(var i=0, sz=str.length; i<sz && i<50;i++)txt +='-';
		//txt +='\n';
	break;
	case 'br':
		txt = str+'  \n';
	break;
	case 'help':
		window.open('http://www.michelf.com/projects/php-markdown/dingus/', 'help', 'width=610,height=420,scrollbars,resizable');
		return;
	break;
	}
	setSel(inputId, txt);

}

function help(){
	window.open('http://www.michelf.com/projects/php-markdown/dingus/', 'help', 'width=610,height=420,scrollbars,resizable');
	return false;
}

/// selection
function getSel(inputId){
	var input=document.getElementById(inputId);
    	input.focus();
	if( input.createTextRange ) {//ie
		return document.selection.createRange().text;
    	} else {
    		var ss = input.selectionStart;
		var se = input.selectionEnd;
		return input.value.substr( ss, se-ss );
		input.value = input.value.substr( 0, len )
	    	 + input.value.substr( len );
		return input.setSelectionRange(len,len);
    	}
}


function setSel(inputId, insText){
	var input=document.getElementById(inputId);
    	input.focus();
    	if( input.createTextRange ) {
      	var rng=document.selection.createRange();
            // ie
		rng.text = insText;
    	} else if( input.setSelectionRange ) {
		var st = input.scrollTop;
    		var ss = input.selectionStart;
		var se = input.selectionEnd;
		input.value = input.value.substr( 0, ss )
	    	+ insText + input.value.substr( se );
		input.setSelectionRange(se+insText.length,se+insText.length);
		input.scrollTop=st;
    	}
    	reloadPreview(inputId);
}
/////

function reloadPreview(inputId) {
	var input = document.getElementById(inputId);
	var html = converter.makeHtml(input.value);
	var preview = document.getElementById(inputId+'_preview');//input.parentNode.lastChild;
	preview.innerHTML = html ;
}
function switchView(inputId){
	//toggleLayer(inputId);
	var inp=document.getElementById(inputId);
	if(inp.style.width=='50%')inp.style.width='100%';
	else inp.style.width='50%';
	toggleLayer(inputId+'_preview');
	//toggleVisibility(inputId+'_tools');
}
function toggleLayer(whichLayer){
	var layer = document.getElementById(whichLayer);
	//layer.style.display = layer.style.display? "":"block";
	if(layer.style.display=='block')layer.style.display='none';
	else layer.style.display='block';
}
function toggleVisibility(whichLayer){
	var vis = document.getElementById(whichLayer);
	if(vis.className=='HTE_tools') vis.className='HTE_tools disabled';
	else vis.className='HTE_tools';
}

function HTE_addEvent(t, ev, fn) {

		if (typeof document.addEventListener != 'undefined')
		{
			t.addEventListener(ev,fn,false);
		}
		else
		{
			t.attachEvent('on' + ev, fn);
		}
}
