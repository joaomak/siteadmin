<?php 
/*
    Dynamic Heading Generator
    By Stewart Rosenberger
    http://www.stewartspeak.com/headings/    

    This script generates PNG images of text, written in
    the font/size that you specify. These PNG images are passed
    back to the browser. Optionally, they can be cached for later use. 
    If a cached image is found, a new image will not be generated,
    and the existing copy will be sent to the browser.

    Additional documentation on PHP's image handling capabilities can
    be found at http://www.php.net/image/    
*/

$font_file = 'Ransom.ttf';
$font_size  = 24; // font size in pts
$font_color = '#444444' ;
$background_color = '#ffffff' ;
$transparent_background  = false ;

session_start();
if(isset($_SESSION['captchaColor']))$font_color=$_SESSION['captchaColor'];
if(isset($_SESSION['captchaBgColor']))$background_color=$_SESSION['captchaBgColor'];
if(isset($_SESSION['captchaFontSize']))$font_size=$_SESSION['captchaFontSize'];
if(isset($_SESSION['captchaFontFile']))$font_file=$_SESSION['captchaFontFile'];

if(isset($_GET['color']))$font_color='#'.$_GET['color'];
if(isset($_GET['bg']))$background_color='#'.$_GET['bg'];
if(isset($_GET['font-size']))$font_size=$_GET['font-size'];
if(isset($_GET['font-file']))$font_file=$_GET['font-file'];

/*
   --------------------------------------------------------------
   Optionally, images can be cached for later use.
   If a cached image is found, a new image will not be generated,
   and the existing copy will be sent to the browser. However, 
   that won't happen unless a WRITABLE "cache" directory is there 
   for the images to be stored.
   --------------------------------------------------------------
*/
putenv('GDFONTPATH=' . realpath('.'));
$cache_images = false ;
$cache_folder = 'cache' ;

function simpleCrypt($txt){
	$str="1234567890abcdefghijklmnopqrstuvwxyz";
	$seq=preg_split('[]',$str); 
	array_pop($seq);
	array_shift($seq);
	$keys=array_flip($seq);
	//print_r($keys);exit;
	
	$crypt=preg_split('[]',$txt); 
	array_pop($crypt);
	array_shift($crypt);
//print_r($crypt);exit;
	$new='';
	foreach($crypt as $c){
		if(key_exists($c, $keys)){
			$pos=$keys[$c];
			$new.=substr($str, -$pos, 1);
		}else $new.=$c;
	}
	return $new;	
}
if(isset($_GET['text']))$_GET['text']=simpleCrypt($_GET['text']);

/*
  ---------------------------------------------------------------------------
   For basic usage, you should not need to edit anything below this comment.
   If you need to further customize this script's abilities, make sure you
   are familiar with PHP and its image handling capabilities.
  ---------------------------------------------------------------------------
*/

$mime_type = 'image/png' ;
$extension = '.png' ;
$send_buffer_size = 4096 ;

// check for GD support
if(!function_exists('ImageCreate'))
    fatal_error('Error: Server does not support PHP image generation') ;

// clean up text
if(empty($_GET['text']))
    fatal_error('Error: No text specified.') ;
    
$text = html_entity_decode($_GET['text']).' ' ;
if(get_magic_quotes_gpc())
    $text = stripslashes($text) ;
$text = javascript_to_html($text) ;

// look for cached copy, send if it exists
$hash = md5(basename($font_file) . $font_size . $font_color .
            $background_color . $transparent_background . $text) ;
$cache_filename = $cache_folder . '/' . $hash . $extension ;
if($cache_images && ($file = @fopen($cache_filename,'rb')))
{
    header('Content-type: ' . $mime_type) ;
    while(!feof($file))
        print(($buffer = fread($file,$send_buffer_size))) ;
    fclose($file) ;
    exit ;
}

// check font availability
$font_found = is_readable($font_file) ;
if(!$font_found)
{
    fatal_error('Error: The server is missing the specified font.') ;
}

// create image
$background_rgb = hex_to_rgb($background_color) ;
$font_rgb = hex_to_rgb($font_color) ;
$dip = get_dip($font_file,$font_size) ;
$box = @ImageTTFBBox($font_size,0,$font_file,$text) ;
$image = @ImageCreate(abs($box[2]-$box[0]),abs($box[5]-$dip)) ;
if(!$box)
{
    fatal_error('Error: The server could not create this heading image.') ;
}

// allocate colors and draw text
$background_color = @ImageColorAllocate($image,$background_rgb['red'],
    $background_rgb['green'],$background_rgb['blue']) ;
$font_color = ImageColorAllocate($image,$font_rgb['red'],
    $font_rgb['green'],$font_rgb['blue']) ;   
ImageTTFText($image,$font_size,0,-$box[0],abs($box[5]-$box[3])-$box[3],
    $font_color,$font_file,$text) ;
// Y was: abs($box[5]-$box[3])-$box[3]
//php 5: $font_size-$box[3]  ??
// lines
$height = abs($box[5]-$dip);
$width = abs($box[2]-$box[0])+2;
//imageline ($image,0,rand(0,$height),$width,rand(0,$height),$font_color);
imageline ($image,0,rand(0,$height),$width,rand(0,$height),$background_color);
for($p=0;$p<30;$p++)imagesetpixel ( $image,rand(0,$width) , rand(0,$height), $font_color);

// set transparency
if($transparent_background)
    ImageColorTransparent($image,$background_color) ;

header('Content-type: ' . $mime_type) ;
ImagePNG($image) ;

// save copy of image for cache
if($cache_images)
{
    @ImagePNG($image,$cache_filename) ;
}

ImageDestroy($image) ;
exit ;


/*
	try to determine the "dip" (pixels dropped below baseline) of this
	font for this size.
*/
function get_dip($font,$size)
{
	$test_chars = 'abcdefghijklmnopqrstuvwxyz' .
			      'ABCDEFGHIJKLMNOPQRSTUVWXYZ' .
				  '1234567890' .
				  '!@#$%^&*()\'"\\/;.,`~<>[]{}-+_-=' ;
	$box = @ImageTTFBBox($size,0,$font,$test_chars) ;
	return $box[3] ;
}


/*
    attempt to create an image containing the error message given. 
    if this works, the image is sent to the browser. if not, an error
    is logged, and passed back to the browser as a 500 code instead.
*/
function fatal_error($message)
{
    // send an image
    if(function_exists('ImageCreate'))
    {
        $width = ImageFontWidth(5) * strlen($message) + 10 ;
        $height = ImageFontHeight(5) + 10 ;
        if($image = ImageCreate($width,$height))
        {
            $background = ImageColorAllocate($image,255,255,255) ;
            $text_color = ImageColorAllocate($image,0,0,0) ;
            ImageString($image,5,5,5,$message,$text_color) ;    
            header('Content-type: image/png') ;
            ImagePNG($image) ;
            ImageDestroy($image) ;
            exit ;
        }
    }

    // send 500 code
    header("HTTP/1.0 500 Internal Server Error") ;
    print($message) ;
    exit ;
}


/* 
    decode an HTML hex-code into an array of R,G, and B values.
    accepts these formats: (case insensitive) #ffffff, ffffff, #fff, fff 
*/    
function hex_to_rgb($hex)
{
    // remove '#'
    if(substr($hex,0,1) == '#')
        $hex = substr($hex,1) ;

    // expand short form ('fff') color
    if(strlen($hex) == 3)
    {
        $hex = substr($hex,0,1) . substr($hex,0,1) .
               substr($hex,1,1) . substr($hex,1,1) .
               substr($hex,2,1) . substr($hex,2,1) ;
    }

    if(strlen($hex) != 6)
        fatal_error('Error: Invalid color "'.$hex.'"') ;

    // convert
    $rgb['red'] = hexdec(substr($hex,0,2)) ;
    $rgb['green'] = hexdec(substr($hex,2,2)) ;
    $rgb['blue'] = hexdec(substr($hex,4,2)) ;

    return $rgb ;
}


/*
    convert embedded, javascript unicode characters into embedded HTML
    entities. (e.g. '%u2018' => '&#8216;'). returns the converted string.
*/
function javascript_to_html($text)
{
    $matches = null ;
    preg_match_all('/%u([0-9A-F]{4})/i',$text,$matches) ;
    if(!empty($matches)) for($i=0;$i<sizeof($matches[0]);$i++)
        $text = str_replace($matches[0][$i],
                            '&#'.hexdec($matches[1][$i]).';',$text) ;

    return $text ;
}

?>
