<?php 
/* simplestats: siteAdmin package */

session_start();
if(isset($_SESSION['uname']))exit;
header('Content-Type: image/jpeg');

include_once('../../config.php');
$logDir = '../../../'.$GLOBALS['dataFolder'].'/.log/';
if(!file_exists($logDir)) mkdir($logDir);

// log hit
if(isset($_GET['href']))loghit($_GET['href'],$logDir);


function writeFile($file_name,$cont, $method="w"){ 
    if ($fp = fopen($file_name, $method)){ 
        $write = ($cont);  
        fwrite($fp, $write);  
        fclose($fp);
        return true;  
    }else return false;        
}
function rfile($file){
	$ponteiro = fopen($file, "r");
    	$conteudo = fread($ponteiro, filesize($file) );
    	fclose($ponteiro);
   	return $conteudo;
}
function loghit($link,$logDir){
	if(!$link)return false;
	$latFile=$logDir .(date("Y")-1).'-'.date("m");
	if(file_exists($latFile)) unlink($latFile);
	$file=$logDir .date("Y-m");
	if(file_exists($file)){
		$c=rfile($file);
		$arr=unserialize(trim($c));
		
		if(isset($arr[$link]))	$arr[$link]+=1;
		else $arr[$link]=1;
		//print_r($arr);exit;
		writeFile($file,serialize($arr));

		return true;
	}else writeFile($file,serialize(array($link=>1)));
	return true;
}

?>
