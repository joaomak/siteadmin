<?php

function rfile($file){
	$ponteiro =	fopen($file, "r");
    	$conteudo =	fread($ponteiro, filesize($file) );
    	fclose($ponteiro);
   	return unserialize($conteudo);
}


/*** OUTPUT ***/
date_default_timezone_set('America/Sao_Paulo');
if(!isset($_GET['date'])) $_GET['date']=date("Y-m");


if(!isset($_GET['path'])) $_GET['path']=dirname(dirname($_SERVER['REQUEST_URI']));
if(substr($_GET['path'],-1)!='/') $_GET['path'].='/';
// read by flash

$logDir = '../'.$GLOBALS['dataFolder'].'/.log/';

$stt=file_list($logDir,false);
//print_r($stt);exit;
if(!empty($stt)){
	sort($stt);

	$output='';
	$ch_title=$overview_grapTag;
	$ch_legend='';
	$statscolors=array('00B366','395BBF','FF3300','FF9900','FFE500','008F00','B30000','B35A00','00487D','BFFFBF');//,'FFBFBF', 'FFDFBF');//'BFE4FF','80FF80','FF8080','FFC080','80C9FF','FFB380','80FFFF','9191FF','006B6B','aaaaaa');
	$meses=array();
	$visitas=array();
	$urls=array();
	$top_urls=array();
	$visitas_url=array();
	$year_total=0;
	$top_month=0;
	$total_months=array();

	$now=explode('-',$_GET['date']);
	$thisyear=$now[0];
	$thismonth=$now[1];


	for($m=1;$m<=12;$m++){
		$year=$thisyear-1;
		$month=$thismonth+$m;
		if($month>12){
			$year=$thisyear;
			$month=$month-12;
		}
		if(strlen($month)==1)$month='0'.$month;
		$file=$year.'-'.$month;
		$mname=dateFormat($file.'-01', '%b', $lang);
		$output.= h2($mname.'/'.$year);
		$t=0;
		$meses[]=$mname;//.'-'.substr($year,-2);

		if(file_exists($logDir.$file)){
			$c=rfile($logDir.$file);
			$arr=($c);
			//$arr=array_slice($c, 0, 9,true);
			//print_r($c);

			$stack=array();
			foreach($arr as $url=>$n){
				//$parte=str_replace($_GET['path'],'', $url);
				//$split=explode('/', $parte);
				//$nome=$split[0];
				//$file_extension = substr(strpos($filename,"/"), 1);
				$output.=$url.':  '.$n.br();
				$t+=$n;
				$top_urls[trim($url)]=$n;
				$stack[]=$n;
				$urls[]=basename($url);
				if(isset($top_urls[$url])) $top_urls[$url]+=1;
				else $top_urls[$url]=1;
			}

			if($t>$top_month)$top_month=$t;
			$visitas_url[]=implode(',',$stack);

		}
		$output.='Total: '.b($t);
		$year_total+=$t;
		$total_months[]=$t;
		/* parcial
		$percent=array();
		foreach($urls)
		$output.='<img src="http://chart.apis.google.com/chart?chtt=Top+urls&amp;chts=000000,12&amp;chs=300x150&amp;chf=bg,s,ffffff&amp;cht=p3&amp;chd=t:100.00&amp;chl=Label+1&amp;chco=0000ff" alt="Google Chart"/>';*/
		$output.=br().br();
	}

	$yaxis=implode('|',$meses);//'Jan|Fev|Mar|Abr|Mai';
	$divide=($top_month/4);
	$xaxis='0|'.round($divide).'|'.round($divide*2).'|'.round($divide*3).'|'.$top_month;
	$glabels=$ch_legend;//implode('|',array_unique($urls));//'url+1|url+2';
	$tdata=implode(',',$total_months);//'38.46,23.07,18.68,20.87,45.05|13.18,13.18,0.00,6.59,54.94';
	$chco=$hex;//'cd600f';//implode(',',$statscolors);//'0000ff,339933';

	// G Charts: http://www.clabberhead.com/googlechartgenerator.html
	$overview_graph= '<img id="overview-chart" class="img-responsive center-block" src="http://chart.apis.google.com/chart?chtt='.$ch_title.'&amp;chts=333333,13&amp;chs=356x200&amp;chf=bg,s,ffffff|c,s,ffffff&amp;chxt=x,y&amp;chxl=0:|'.$yaxis.'|1:|'.$xaxis.'&amp;cht=bvs&amp;chd=t:'.$tdata.'&amp;chdl='.urlencode($glabels).'&amp;chco='.$chco.'&amp;chds=0,'.$top_month.'&amp;chbh=23&amp;chg=110,25,1,7" alt="'.$loadingChartTag.'" />';

	//http://chart.apis.google.com/chart?chtt=Visitas+por+Mes&chts=666666,12&chs=400x200&chf=bg,s,ffffff|c,s,ffffff&chxt=x,y&chxl=0:|Mai |Jun |Jul |Ago |Set |Nov |1:|0.00|5|10&cht=bvs&chd=t:1,1,1|1,1,3,4|1,1,1|1,1,3,4|1,1,1,2,5|1,1,1,2,5&chdl=site%3A+1%7C%3Fcmd%3DfileManager%26pop%3D1%3A+1%7Cpages%3A+1%7Cpost%3A+3%7Cglobo%3A+4%7Cpost%3A+1%7Cprimeira-reuniaatilde%3Bo-com-os-arquitetos%3A+1%7Cpages.html%3A+2%7C50-ways%3A+5&chco=00B366,395BBF,FF3300,FF9900,FFE500,008F00,B30000,B35A00,00487D,BFFFBF,FFBFBF,FFDFBF&chbh=23


	//'<img src="http://chart.apis.google.com/chart?chtt=Visitas+por+Mes&amp;chts=666666,12&amp;chs=400x200&amp;chf=bg,s,ffffff|c,s,ffffff&amp;chxt=x,y&amp;chxl=0:|Jan|Fev|Mar|Abr|Mai|1:|0.00|20.50|41.00&amp;cht=bvs&amp;chd=t:85.36,51.21,41.46,46.34,100.00&amp;chdl=Legend+Dataset+1&amp;chco=0000ff&amp;chbh=23" alt="Google Chart"/>'
	if($pg=='stats')$content.= $overview_graph.$output;
	//die($content);
}
?>
