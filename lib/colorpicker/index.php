<?php
/* ColorPicker by
http://ecritters.biz/colorselector/
Image picker by Joao Makray 
*/

include "../../config.php";
include_once "../../inc/lang/".$lang.".php";

?>
<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>
Color Picker
</title>

<meta http-equiv="content-type" content="application/xhtml+xml; charset=iso-8859-1" />
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet" />
<style type="text/css">
@import "../../assets/styles.css";

html,body{overflow: hidden}
body{
padding:0;
margin:0;
background:#333;
color:#fff
}
#control{
background:#d7d4d0;
color:#000;
border:1px solid #999;
padding:4px;
text-align:right;
width:100%;
position:absolute;
bottom: 0;
}
input{
background:#222;
border:1px solid #666;
color:#fff
}

#div{
padding:0
}

</style>
</head>
<body id="body">
<input type="text" id="color" name="color"  />
<input type="hidden" id="img" name="img"  />

<script type="text/javascript">
wo=parent.window.opener ;
if(wo && !wo.closed ){ 
	var campo = wo.document.getElementById(wo.returnField);
	if(campo){
    		var hex = campo.value;
    		document.getElementById('color').value=hex;
	}   
}
    	
function save(){
	var color=document.getElementById('color').value;
    	if(wo && !wo.closed ){ 
        	var campo = wo.document.getElementById(wo.returnField);
        	if(campo){
            		campo.value=(color);
            		campo.nextSibling.style.color=color;
            		wo.focus();
            		window.close();
            		return false; 
		}   
    	}
    	return false; 
   	// parent.location='view.php?file='+path; 
}
function fileManager (){
    var path='../../?cmd=fileManager&class=popup';
    var winW=714 ;
    var winH=400;
    var iMyWidth;
	var iMyHeight;
    var toolbar=0;
	iMyWidth = (window.screen.width/2) - ((winW/2)+ 10); //half the screen width minus half the new window width (plus 5 pixel borders).
	iMyHeight = (window.screen.height/2) - ((winH/2) + 50); //half the screen height minus half the new window height (plus title and status bars).
	var win2 = window.open(path,"filem","status,toolbar="+toolbar+",height="+winH+",width="+winW+",resizable=yes,left=" + iMyWidth + ",top=" + iMyHeight + ",screenX=" + iMyWidth + ",screenY=" + iMyHeight + ",scrollbars=yes");
    win2.opener=window;
    win2.focus();
}
function imagePicker(){
	window.returnField='img';
	fileManager();
	return false
}
function callback(){
	var save=document.getElementById('color').value;
	
	var path=document.getElementById('img').value;
	var picker = new FlashObject("cs.swf", "cs", "300", "300", 6, "000");
	picker.addVariable("img", path);
	picker.write("div");
	
	var input= document.createElement('input');
	input.id='color';
	input.type='hidden';
	input.value=save;
	document.getElementById('body').appendChild(input);
	
	document.getElementById('div').style.height='auto';
	window.resizeTo(333, 436);
}
function setColor(hex){
	document.getElementById('body').style.background=hex;
	document.getElementById('color').value=hex;
}

</script>
<script src="swfobject.js" type="text/javascript"></script> 
<script src="colorpicker.js" type="text/javascript"></script>

<div id="control">
<button class="cancel btn btn-default pull-left" onclick="return imagePicker();" id="picker" title="<?php echo $imagePickTag; ?>"><i class="icon-picture"></i></button>
<button onclick="save()" class="save btn btn-default" ><i class=" icon-ok-sign"></i> <span>OK</span></button> 
<button class="cancel btn btn-default" onclick="window.close()"><span><?php echo $cancelTag; ?></span></button>
</div>
</body>
</html>
