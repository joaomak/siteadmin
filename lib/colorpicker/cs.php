<?php

$_GET['size']=300;

if(
isset($_REQUEST['img']) && 
isset($_REQUEST['colorx']) && 
isset($_REQUEST['colory'])
){
	$exPath = explode('/',$_GET['img']);
	$filename = array_pop($exPath);
	$thumb =  '../../../'.$_GET['img'];


	$rgb=getColorAt($thumb, $_REQUEST['colorx'], $_REQUEST['colory']);
	echo "&colorHex=".$rgb. '&';
	exit;
}


function getColorAt($file, $x, $y) {

	if( !is_file($file) )
		die('1'); //return false;

	$file_img = getImageSize($file);
	
	switch ($file_img[2]) {
		case 1: //GIF
			$srcImage = imagecreatefromgif($file);
			break;
		case 2: //JPEG
			$srcImage = imagecreatefromjpeg($file);
			break;
		case 3: //PNG
			$srcImage = imagecreatefrompng($file);
			break;
		
		default:
			die('2'); //return false;
	}
	
	if(!$srcImage) 
		die('3'); //return false;
		
	$color_index  = imagecolorat($srcImage, $x, $y);
	$colorrgb = imagecolorsforindex($srcImage, $color_index);
			foreach($colorrgb as $k => $v) {
				$t[$k] = dechex($v);
				if( strlen($t[$k]) == 1 ) {
					if( is_int($t[$k]) ) {
						$t[$k] = $t[$k] . "0";
					} else {
						$t[$k] = "0" . $t[$k];
					}
				}
			}
	$rgb = strtoupper($t['red'] . $t['green'] . $t['blue']);
	//print_r($rgb);exit;
	return $rgb;
}
?>
