<?php
if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start("ob_gzhandler"); else ob_start();
include ("inc/pages.php");
?>
<!doctype html>
<html lang="<?php echo $lang; ?>">
<!-- siteAdmin CMS: www.getsiteadmin.com -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?php echo $encoding; ?>" />
    <base href="<?php echo $BASE; ?>" />
    <title>[<?php echo str_replace('www.','',$_SERVER['HTTP_HOST']); ?>] siteAdmin</title>

    <meta name="author" content="Oktala" />
    <meta name="robots" content="noindex,nofollow" />
    <link rel="shortcut icon" href="img/favicon.ico" type="image/ico" />
    <link rel="apple-touch-icon" href="img/apple-touch-icon.png" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

    <script type="text/javascript" >_lang = "<?php echo $lang; ?>";</script>
    <?php
    // CSS
    foreach($css as $csspath) echo '    <link href="'.$csspath.'" rel="stylesheet" />'."\n";
    ?>
    <style type="text/css" ><?php echo $colors; ?></style>
    <!--[if lt IE 7]><link rel="stylesheet" href="assets/ie.css" media="all" type="text/css" /><![endif]-->
</head>

<body id="<?php echo $pg; if(isset($_GET['class']))echo '" class="'.$_GET['class'];  ?>">

<?php include('inc/'.$rootTemplate); ?>

  <?php
     // JS
    foreach($js as $jspath) echo '    <script type="text/javascript" src="'.$jspath.'"></script>'."\n";
  ?>
</body>
</html>
