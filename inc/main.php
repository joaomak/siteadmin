
<div id="wrap" class="<?php echo $lang; ?>">

    <div id="header" class="bgcolor"><?php if(isset($h))echo $h; ?>
    <?php if(isset($demo))echo '<div id="demo">'.$demoTag.'</div>'; ?>

    <a id="toggleMenu" class="icon-reorder"></a>
    </div>

    <div id="sidebar" data-spy="affix" data-offset-top="130" data-offset-bottom="70"><?php if(isset($s))echo $s; ?></div>

    <div id="content"><div id="shadow">
        <noscript class="alert alert-danger"><?php echo $noscriptTag; ?></noscript>
        <?php if(isset($content))echo $content; ?>
    </div>
    </div>

    <div id="meta" class="extra"><?php if(isset($meta)&&$meta)echo $meta; ?></div>
    <div id="comments" class="extra"><?php if(isset($comments)&&$comments)echo $comments; ?></div>

    <div id="footer">
    	<a href="http://www.getsiteadmin.com/" id="logo" title="getsiteadmin.com"><img src="img/logo.gif" alt="siteAdmin" /><small>v<?php echo $version; ?></small></a>
    	  <a href="?lang=en" id="en-lang" title="English"><i class="icon-ok-sign"></i> English</a> |
        <a href="?lang=pt" id="pt-lang" title="Português"><i class="icon-ok-sign"></i> Portugu&ecirc;s</a>
    </div>
</div>
