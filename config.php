<?php
/************************\
        siteAdmin
\************************/

$lang =             "pt_BR"; // en
$dataFolder =       "data";
$filesFolder =      "files";
$openFolder =	    "download"; // folder inside the filesFolder with public access
$hex =		    	"#789fca"; // theme color (Ex: #46a04b, #e5af10, #cd600f)
$username =			""; //your getsiteadmin username

/****** end config ******/
?>
